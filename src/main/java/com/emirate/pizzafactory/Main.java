package com.emirate.pizzafactory;

import com.emirate.pizzafactory.type.Pizza;

public class Main {
 
	public static void main(String[] args) {
		PizzaFactory factory = new PizzaFactory();
		PizzaStore store = new PizzaStore(factory);
		Pizza pizza;

		pizza = store.orderPizza("cheese");
		System.out.println("We ordered a " + pizza.getName() + "\n");
		System.out.println(pizza);

		System.out.println("--------------------------------------------\n");
 
		pizza = store.orderPizza("veggie");
		System.out.println("We ordered a " + pizza.getName() + "\n");
		System.out.println(pizza);
	}
}
