package com.emirate.pizzafactory;

import com.emirate.pizzafactory.type.CheesePizza;
import com.emirate.pizzafactory.type.PepperoniPizza;
import com.emirate.pizzafactory.type.Pizza;
import com.emirate.pizzafactory.type.VeggiePizza;

public class PizzaFactory {
	public Pizza createPizza(String type) {
		Pizza pizza = null;

		if (type.equals("cheese")) {
			pizza = new CheesePizza();
		} else if (type.equals("pepperoni")) {
			pizza = new PepperoniPizza();
		} else if (type.equals("veggie")) {
			pizza = new VeggiePizza();
		}

		return pizza;
	}
}
